<?php
//append the walker menu
include( 'custom-walker-menu.php' );

function tcntheme_setup(){
    register_nav_menu( 'primary', __('Navigation Menu', 'tcntheme' ) );
};
add_action('after_setup_theme', 'tcntheme_setup');

function tcntheme_wp_title( $title, $sep ) {
    global $paged, $page;
    
    if ( is_feed() )
        return $title;
    
    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );
    
    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";
    
    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) );
    
    return $title;
}
add_filter( 'wp_title', 'tcntheme_wp_title', 10, 2 );

?>
