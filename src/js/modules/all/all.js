yOSON.AppCore.addModule("responsive-user-menu", function(){
    var st = {
        lnkMenu: ".b-responsive-user-menu .icon-sandwich",
        overlay: ".b-responsive-user-menu__overlay",
        menusToShow: ".b-responsive-user-menu__down, .b-responsive-user-menu__down .b-menu"
    };
    var DOM = {};
    var catchDOM = function(){
        DOM.lnkMenu = $(st.lnkMenu);
        DOM.menusToShow = $(st.menusToShow);
        DOM.overlay = $(st.overlay);
    };
    var subscribeEvents = function(){
        DOM.lnkMenu.on("click", events.showMenus);
        DOM.overlay.on("click", events.hideMenus);
    };
    var events = {
        showMenus: function(evt){
           DOM.menusToShow.fadeIn();
           events.showOverlay();
           evt.preventDefault();
        },
        showOverlay: function(){
           DOM.overlay.fadeIn();
        },
        hideMenus: function(){
           DOM.overlay.fadeOut();
           DOM.menusToShow.fadeOut();
        }
    };
    return {
        init: function(){
            $(function(){
                catchDOM();
                subscribeEvents();
            });
        }
    };
});
yOSON.AppCore.runModule("responsive-user-menu");
