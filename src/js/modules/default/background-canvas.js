var imageUrl = "../../static/img/lite-grass.png";

var createImageElementBySrc = function(urlSrc, onloaded){
    var newImageElement = document.createElement("img");
    newImageElement.src = urlSrc;
    newImageElement.onload = function(){
        onloaded.call(this, newImageElement);
    };
    return newImageElement;
};

window.onload = function(){
    createImageElementBySrc(imageUrl, function(imageCreated){
        var canvasSelf = document.getElementById("canvas");
        var canvasContext = canvasSelf.getContext("2d");
        var canvasImageTexture = canvasContext.createPattern(imageCreated, "repeat");

        canvasContext.save();
        canvasContext.fillStyle = canvasImageTexture;
        canvasContext.fillRect(0, 0, 2000, 2000);
        canvasContext.closePath();

        canvasContext.stroke();
        canvasContext.restore();
    });
};
