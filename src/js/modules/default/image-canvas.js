var canvasSelf = document.getElementById("canvas");
var canvasContext = canvasSelf.getContext("2d");
var imageUrl = "../../static/img/hello_world.jpg";

var createImageElementBySrc = function(urlSrc, onloaded){
    var newImageElement = document.createElement("img");
    newImageElement.src = urlSrc;
    newImageElement.onload = function(){
        onloaded.call(this, newImageElement);
    };
    return newImageElement;
};

window.mergeObjects = function(objectA, objectB){
    var objResult = {};
    for(var keyObject in objectA){
        if( typeof objectB[keyObject] !== "undefined"){
            if(typeof objectB[keyObject] === "object"){
                objResult[keyObject] = window.mergeObjects(objectA[keyObject], objectB[keyObject]);
            } else {
                objResult[keyObject] = objectB[keyObject];
            }
        } else {
            objResult[keyObject] = objectA[keyObject];
        }
    }
    return objResult;
};

window.renderImage = function(imgElement, options){
    var defaults = {
        PositionX: 0,
        PositionY: 0,
        width: imgElement.width,
        height: imgElement.height,
        clip: {
            PositionX: 0,
            PositionY: 0,
            width: imgElement.width,
            height: imgElement.height
        }
    };
    var settings = window.mergeObjects(defaults, options);
    canvasContext.drawImage(imgElement,
        settings.clip.PositionX,
        settings.clip.PositionY,
        settings.clip.width,
        settings.clip.height,
        settings.PositionX,
        settings.PositionY,
        settings.width,
        settings.height
    );
};

var renderWithDimension = function(imgSelf, PositionX, PositionY){
    canvasContext.drawImage(imgSelf, PositionX, PositionY);
};

window.onload = function(){
    createImageElementBySrc(imageUrl, function(imageCreated){
        canvasContext.save();
        window.gggg = imageCreated;
        window.renderImage(imageCreated, {
            PositionX: 5,
            PositionY: 5
        });
        canvasContext.closePath();

        canvasContext.stroke();
        canvasContext.restore();
    });
};
