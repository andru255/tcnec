var general = require("general");

var SRC_PATH = general.SRC_PATH;
var BUILD_PATH = general.BUILD_PATH;
var DEPLOY_PATH = general.DEPLOY_PATH;

var path = {
    statics: {
        src: SRC_PATH + "static/**/*.*",
        build: BUILD_PATH + "static/"
    },
    sprites: {
        src: SRC_PATH + "static/img/_sprite/*.png",
        build: {
            stylus: SRC_PATH + "stylus/_mixin/",
            image: SRC_PATH + "static/img/"
        }
    },
    stylus:{
        watch: [
            SRC_PATH + "stylus/*.styl",
            SRC_PATH + "stylus/**/*.styl"
        ],
        src: [
            SRC_PATH + "stylus/*.styl",
            SRC_PATH + "stylus/**/*.styl",
            "!" + SRC_PATH + "stylus/_**/*.styl",
            "!" + SRC_PATH + "stylus/**/_**/*.styl"
        ],
        build: BUILD_PATH + "css/"
    },
    fonts:{
        src: [
            SRC_PATH + "stylus/fonts/**/*.*",
        ],
        build: BUILD_PATH + "css/fonts/"
    },
    jade:{
        watch: [
            SRC_PATH + "jade/*.jade",
            SRC_PATH + "jade/**/*.jade"
        ],
        src: [
            SRC_PATH + "jade/*.jade",
            SRC_PATH + "jade/**/*.jade",
            "!" + SRC_PATH + "jade/_**/*.jade",
            "!" + SRC_PATH + "jade/**/_**/*.jade"
        ],
        build: BUILD_PATH
    },
    js:{
        watch: [
            SRC_PATH + "js/*.js",
            SRC_PATH + "js/**/*.js"
        ],
        src: [
            SRC_PATH + "js/*.js",
            SRC_PATH + "js/**/*.js",
            "!" + SRC_PATH + "js/_**/*.js",
        ],
        build: BUILD_PATH + "js/"
    },
    watch: {
        stylus: path.stylus.watch,
        jade:   path.jade.watch,
        js:   path.js.watch
    }
};
