var general = require("general");
var SRC_PATH = general.SRC_PATH;
var BUILD_PATH = general.BUILD_PATH;
var options = {
    sprites:{
      algorithm: 'binary-tree',
      imgName: 'tcn-sprite.png',
      cssName: 'tcn-sprite.styl',
      imgPath: '../../../static/img/tcn-sprite.png'
    },
    jade :{
       pretty: true,
       compileDebug: true
    },
    bemjast: {
       params: process.argv,
       blockJadePath: SRC_PATH + "jade/_blocks/",
       blockStylusPath: SRC_PATH + "stylus/_blocks/",
       moduleJadePath: SRC_PATH + "jade/modules/",
       moduleStylusPath: SRC_PATH + "stylus/modules/",
       pathAllModulesStylus: SRC_PATH + "stylus/modules/all/"
    },
    browserSync: {
      server: {
        baseDir: "./",
        directory: true
      },
      port: 8000
    }
};
module.exports = options;
