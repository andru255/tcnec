var general = require("./general");
var SRC_PATH = general.SRC_PATH;
var BUILD_PATH = general.BUILD_PATH;

var gulp = require("gulp"),
    stylus = require("gulp-stylus"),
    jade = require("jade"),
    gulpJade = require("gulp-jade"),
    debug = require("gulp-debug"),
    gutil = require("gulp-util"),
    spritesmith = require("gulp.spritesmith"),
    spriteEngine = require("phantomjssmith"),
    parseArgs = require("minimist"),
    browserSync = require("browser-sync"),
    rupture = require("rupture"),
    jeet = require("jeet"),
    gulpRename = require("gulp-rename"),
    runSequence = require("run-sequence"),
    runTask = function(taskName){
        var taskSelf = require("./tasks/"+ taskName);
        var globals = {
            SRC_PATH: SRC_PATH,
            BUILD_PATH: BUILD_PATH
        };
        var librariesForTask = {
            gutil: gutil,
            parseArgs: parseArgs
        };
        return new taskSelf(gulp, options, librariesForTask);
    },
    del = require('del'),
    runMainTask = function(taskName, options){
        var taskSelf = require("./tasks/"+ taskName + "/main.js");
        var librariesForTask = {
            gutil: gutil,
            parseArgs: parseArgs
        };
        return new taskSelf(gulp, options, librariesForTask);
    };

/** options **/
var options = {
    sprites:{
      algorithm: 'binary-tree',
      imgName: 'tcn-sprite.png',
      cssName: 'tcn-sprite.styl',
      imgPath: '../../tcn/static/img/tcn-sprite.png'
    }
};
/**********paths************/
var path = {
    info: {
        src: SRC_PATH + "info/**/*.*",
        build: BUILD_PATH
    },
    statics: {
        src: SRC_PATH + "static/**/*.*",
        build: BUILD_PATH + "static/"
    },
    sprites: {
        src: SRC_PATH + "static/img/_sprite/*.png",
        build: {
            stylus: SRC_PATH + "stylus/_mixin/",
            image: BUILD_PATH + "static/img/"
        }
    },
    stylus:{
        watch: [
            SRC_PATH + "stylus/*.styl",
            SRC_PATH + "stylus/**/*.styl"
        ],
        src: [
            SRC_PATH + "stylus/*.styl",
            SRC_PATH + "stylus/**/*.styl",
            "!" + SRC_PATH + "stylus/_**/*.styl",
            "!" + SRC_PATH + "stylus/**/_**/*.styl"
        ],
        build: BUILD_PATH + "css/"
    },
    fonts:{
        src: [
            SRC_PATH + "stylus/fonts/**/*.*",
        ],
        build: BUILD_PATH + "css/fonts/"
    },
    jade:{
        watch: [
            SRC_PATH + "jade/*.jade",
            SRC_PATH + "jade/**/*.jade"
        ],
        src: [
            SRC_PATH + "jade/*.jade",
            SRC_PATH + "jade/**/*.jade",
            "!" + SRC_PATH + "jade/_**/*.jade",
            "!" + SRC_PATH + "jade/**/_**/*.jade"
        ],
        build: BUILD_PATH
    },
    js:{
        watch: [
            SRC_PATH + "js/*.js",
            SRC_PATH + "js/**/*.js",
            SRC_PATH + "js/**/**/*.js"
        ],
        src: [
            SRC_PATH + "js/*.js",
            SRC_PATH + "js/**/*.js",
            "!" + SRC_PATH + "js/_**/*.js",
        ],
        build: BUILD_PATH + "js/"
    }
};

var watchPaths = {
        stylus: path.stylus.watch,
        jade:   path.jade.watch,
        js:   path.js.watch
};

/**************tasks*************/
gulp.task("clean", function(cb){
    return del(BUILD_PATH, {
        force: true
    }, cb);
});

gulp.task("2css", function(){
    return gulp.src(path.stylus.src)
        .pipe(stylus({
            use:[jeet(), rupture()],
            compress: true
        }))
        .pipe(gulp.dest(path.stylus.build));
});

gulp.task("2html", function(){
    return gulp.src(path.jade.src)
        .pipe(gulpJade({
            pretty: true,
            compileDebug: true
        }))
        .pipe(gulp.dest(path.jade.build));
});

gulp.task("2js", function(){
    return gulp.src(path.js.src)
        .pipe(gulp.dest(path.js.build));
});

gulp.task("copy_statics", function(){
    return gulp.src(path.statics.src)
        .pipe(gulp.dest(path.statics.build));
});

gulp.task("copy_statics:imgs", function(){
    return gulp.src(SRC_PATH + 'static/img/*.*')
        .pipe(gulp.dest(BUILD_PATH + 'static/img/'));
});

gulp.task("copy_fonts", function(){
    return gulp.src(path.fonts.src)
        .pipe(gulp.dest(path.fonts.build));
});

gulp.task("sprites", function(){
    var spriteSelf = gulp.src(path.sprites.src)
        .pipe(spritesmith(options.sprites));
    spriteSelf.img.pipe(gulp.dest(path.sprites.build.image));
    spriteSelf.css.pipe(gulp.dest(path.sprites.build.stylus));
    gulp.task("copy_statics:imgs");
});

//** utily for bem command
runMainTask("bemjast", {
    params: process.argv,
    blockJadePath: SRC_PATH + "jade/_blocks/",
    blockStylusPath: SRC_PATH + "stylus/_blocks/",
    moduleJadePath: SRC_PATH + "jade/modules/",
    moduleStylusPath: SRC_PATH + "stylus/modules/",
    pathAllModulesStylus: SRC_PATH + "stylus/modules/all/"
});

gulp.task("browser-sync", function(){
    browserSync({
      server: {
        baseDir: "./",
        directory: true
      },
      port: 8000
    });
});

//tasks for wp theme

gulp.task("info", function(){
    return gulp.src(path.info.src)
        .pipe(gulp.dest(path.info.build));
});

gulp.task("wp2php", function(){
    return gulp.src(path.jade.src)
        .pipe(gulpJade({
            pretty: true,
            compileDebug: true
        }))
        .pipe(gulpRename(function(filePath){
            filePath.basename = filePath.dirname;
        }))
        .pipe(gulpRename({
            dirname: "",
            extname: ".php"
        }))
        .pipe(gulp.dest(path.jade.build));
});

var globalCSSDirectory = "modules/all";

gulp.task("wp2css", function(){
    return gulp.src(path.stylus.src)
        .pipe(stylus({
            use:[jeet(), rupture()],
            compress: true
        }))
        .pipe(gulpRename(function(filePath){
            if(filePath.dirname.indexOf("modules/all") > -1){
                filePath.dirname = "all-" + filePath.basename;
            }
        }))
        .pipe(gulpRename(function(filePath){
            filePath.basename = filePath.dirname;
        }))
        .pipe(gulpRename({
            dirname: ""
        }))
        .pipe(gulp.dest(path.stylus.build));
});

gulp.task("wp2js", function(){
    return gulp.src(path.js.src)
        .pipe(gulp.dest(path.js.build));
});

gulp.task("wpTasks", function(callback){
    runSequence('clean',
                ["info",
                 "sprites",
                 "copy_statics:imgs",
                 "wp2php",
                 "wp2css",
                 "wp2js",
                 "copy_statics",
                 "copy_fonts"], callback);
});
// end wp tasks

gulp.task("watch", function(){
    gulp.watch(watchPaths.jade, ["2html", browserSync.reload]);
    gulp.watch(watchPaths.stylus, ["2css", browserSync.reload]);
    gulp.watch(watchPaths.js, ["2js", browserSync.reload]);
});

gulp.task("default", ["2css", "2html", "2js", "copy_statics", "copy_fonts"]);
gulp.task("wp", ["wpTasks"]);
gulp.task("deploy", ["default", "cptotarget"]);
gulp.task("server", ["browser-sync", "watch"]);
