# README #

Pasos para instanciar el portal
### Que se necesita instalar? ###

- nodejs v0.12.0

### Cómo establecerlo? ###

* Hacer un clon del repo
* Ingresar al repo
* Ejecutar en consola lo siguiente:
* npm install
* npm install -g gulp
* gulp server

### Dónde se ven las vistas ? ###
* Por defecto se encuentran todas aqui http://localhost:8000/build/modules/
* Toda vista tiene un index.html donde muestra el avance.
* En curso el lado JS

